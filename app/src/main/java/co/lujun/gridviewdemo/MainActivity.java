package co.lujun.gridviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private GridView mGridView;
    private List<Integer> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGridView = (GridView) findViewById(R.id.gridview);
        mList = new ArrayList<Integer>();
//        mList.add(R.drawable.p1);
//        mList.add(R.drawable.p2);
//        mList.add(R.drawable.p3);
//        mList.add(R.drawable.p4);
//        mList.add(R.drawable.p5);
//        mList.add(R.drawable.p6);
//        mList.add(R.drawable.p7);
//        mList.add(R.drawable.p8);
//        mList.add(R.drawable.p9);

        GridImageAdapter adapter = new GridImageAdapter(this, mList);
        adapter.setOnOperateListener(new GridImageAdapter.OnOperateListener() {
            @Override
            public void onAddImage() {
                mList.add(R.drawable.p1);
            }

            @Override
            public void onDeleteImage(int position) {
                mList.remove(position);
            }
        });

        mGridView.setAdapter(adapter);
    }
}
